import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Please input first number: ");
        int input1 = input.nextInt();
        System.out.print("Please input second number: ");
        int input2 = input.nextInt();
        if (input1 <= input2) {
            while (input2 >= input1) {
                System.out.print(input1 + " ");
                input1++;
            }
        } else {
            System.out.println("Error");
        }
    }
}
