import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int type;
        int num;
        while (true) {
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            type = sc.nextInt();
            if (type == 1) {
                System.out.print("Please input number: ");
                num = sc.nextInt();
                int num1 = 0;
                while (num1 < num) {
                    int num2 = 0;
                    while (num2 <= num1) {
                        System.out.print("*");
                        num2++;
                    }
                    System.out.println();
                    num1++;
                }
            } else if (type == 2) {
                System.out.print("Please input number: ");
                num = sc.nextInt();
                for (int i = num; i > 0; i--) {
                    for (int j = 0; j < i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            } else if (type == 3) {
                System.out.print("Please input number: ");
                num = sc.nextInt();
                for (int i = 0; i < num; i++) {
                    for (int j = 0; j < i; j++) {
                        System.out.print(" ");
                    }
                    for (int j = 0; j < num - i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            } else if (type == 4) {
                System.out.print("Please input number: ");
                num = sc.nextInt();
                for (int i = num; i >= 0; i--) {
                    for (int j = 0; j < i; j++) {
                        System.out.print(" ");
                    }
                    for (int j = 0; j < num - i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            } else if (type == 5) {
                System.out.println("Bye bye!!!");
                break;
            } else {
                System.out.println("Error: Please input number between 1-5");
            }
        }

    }
}
