import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int input1;
        int Sum = 0;
        double Avg = 0;
        int count = 0;

        do {
            System.out.print("Please input number: ");
            input1 = input.nextInt();
            if (input1 == 0)
                break;
            Sum = Sum + input1;
            count++;
            Avg = Sum / count;
            System.out.println("sum: " + Sum + "," + " Avg: " + Avg);
        } while (input1 != 0);
        System.out.println("Bye");
    }

}
