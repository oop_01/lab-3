import java.util.Scanner;

public class Problem5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str;
        while (true) {
            System.out.print("Please input : ");
            str = sc.next();

            if(str.trim().equalsIgnoreCase("bye")) {
                System.out.println("Exit Program");
                System.exit(0);
            }
            System.out.println(str);
        }
    }
}
