public class Problem15 {
    public static void main(String[] args) {
        int num = 5;
        while (num > 0) {
            int num2 = 1;
            while (num >= num2) {
                System.out.print(num2);
                num2++;
            }
            System.out.println();
            num--;
        }
    }

}
