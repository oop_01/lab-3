import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input n: ");
        int num;
        int count = 1;
        num = sc.nextInt();
        while (count <= num) {
            int count2 = 1;
            while (count2 <= num) {
                System.out.print(count2);
                count2++;
            }
            System.out.println();
            count++;
        }
    }

}
